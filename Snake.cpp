#include "Snake.hpp"

Snake::Snake()
{
}

Snake::Snake(Cell &head_, std::vector<Cell> &body_)
{
    head = head_;

    for (const Cell &bodyCell : body_)
        addCell(bodyCell);
    
    score = 0;
}

void Snake::moveBody()
{
    for (int i = body.size() - 1; i > 0; i--)
        body[i] = body[i - 1];

    body[0].x = head.x;
    body[0].y = head.y;
}

void Snake::moveHead(int direction)
{
    switch (direction)
    {
    case 'W':
    case 'w':
        --head.y;
        break;
    case 'A':
    case 'a':
        --head.x;
        break;
    case 'S':
    case 's':
        ++head.y;
        break;
    case 'D':
    case 'd':
        ++head.x;
        break;
    }
}

void Snake::move(int direction)
{
    moveBody();
    moveHead(direction);
}

void Snake::addCell()
{
    body.push_back(Cell(body.back()));
}

void Snake::addCell(const Cell &cell)
{
    body.push_back(cell);
}

bool Snake::checkDeath(int size)
{
    if (head.x < 0 || head.y < 0 || head.x >= size || head.y >= size)
        return true;

    for (int i = 0; i < body.size(); i++)
        if (body[i] == head)
            return true;

    return false;
}
