#pragma once

#include "Globals.hpp"

#include "Cell.hpp"

class Snake
{
public:
    Snake();
    Snake(Cell& head, std::vector<Cell>& body);

    void moveBody();
    void moveHead(int direction);
    void move(int direction);
    void addCell();
    void addCell(const Cell& cell);
    bool checkDeath(int size);

    std::vector<Cell> body;
    Cell head;
    int score;
};