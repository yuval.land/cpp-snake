#include "Game.hpp"

std::vector<std::vector<Cell>> Game::board;

bool Game::isAppleLegal(const Cell &apple, const Snake &snake)
{
    if (snake.head == apple)
        return false;

    for (const Cell &bodyCell : snake.body)
        if (bodyCell == apple)
            return false;

    return true;
}

void Game::generateApple(Cell &apple, const Snake &snake)
{
    do
        apple = {rand() % size, rand() % size};
    while (!isAppleLegal(apple, snake));
    apple.color = Cell::APPLE_CELL;
}

void Game::eatApple(Cell &apple, Snake &snake)
{
    generateApple(apple, snake);
    snake.addCell();
    snake.score++;
}

void Game::updateBoard(const Cell &apple, const Snake &snake)
{
    for (std::vector<Cell> &row : board)
        for (Cell &cell : row)
            cell = Cell();

    for (const Cell &bodyCell : snake.body)
        set(bodyCell);
    
    set(apple);
    set(snake.head);
}

void Game::initBoard(Cell &apple, Snake &snake)
{
    for (int i = 0; i < size; i++)
    {
        std::vector<Cell> row;
        for (int j = 0; j < size; j++)
            row.push_back(Cell());
        board.push_back(row);
    }

    // Default Snake locations
    Cell head = Cell(10, 14, Cell::SNAKE_HEAD);
    std::vector<Cell> body = {
        Cell(10, 15, Cell::SNAKE_CELL),
        Cell(10, 16, Cell::SNAKE_CELL),
        Cell(10, 17, Cell::SNAKE_CELL)
    };
    snake = Snake(head, body);

    generateApple(apple, snake);
}

void Game::set(const Cell &cell)
{
    board[cell.y][cell.x] = cell;
}

void Game::print()
{
    for (const std::vector<Cell> &row : board)
    {
        for (const Cell &cell : row)
            std::cout << cell;
        std::cout << "\r\n\e[0m";
    }
}