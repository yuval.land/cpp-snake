#include "Cell.hpp"

Cell::Cell()
{
    x = y = -1;
    color = EMPTY_CELL; 
}

Cell::Cell(int x_, int y_, Color color_)
    : x(x_), y(y_), color(color_)
{
}

Cell::Cell(const Cell &cell)
{
    x = cell.x;
    y = cell.y;
    color = cell.color;
}

Cell &Cell::operator=(const Cell &cell)
{
    x = cell.x;
    y = cell.y;
    color = cell.color;

    return *this;
}

bool Cell::operator==(const Cell &cell) const
{
    return cell.x == x && cell.y == y;
}

std::ostream &operator<<(std::ostream &os, const Cell &cell)
{
    os << "\e[" << cell.color << "m  ";

    return os;
}