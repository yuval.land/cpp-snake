#pragma once

#include "Globals.hpp"

class Cell
{
public:
    typedef enum
    {
        EMPTY_CELL = 40,
        APPLE_CELL = 101,
        SNAKE_CELL = 102,
        SNAKE_HEAD = 44,
        SNAKE_DEAD = 41,
        RESET = 0,
    } Color;

    Cell();
    Cell(int x, int y, Color color = EMPTY_CELL);
    Cell(const Cell &);

    Cell &operator=(const Cell &cell);
    bool operator==(const Cell &cell) const;
    friend std::ostream &operator<<(std::ostream &os, const Cell &cell);

    int x;
    int y;
    Color color;
};
