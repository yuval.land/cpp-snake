Source: Source.o Cell.o Snake.o Game.o 
	g++ -g *.o -o Snake -lncurses


Cell.o: Cell.cpp Cell.hpp
	g++ -g -c Cell.cpp

Snake.o: Snake.cpp Snake.hpp
	g++ -g -c Snake.cpp

Game.o: Game.cpp Game.hpp
	g++ -g -c Game.cpp

clean:
	rm *.o