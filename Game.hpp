#pragma once

#include "Globals.hpp"

#include "Cell.hpp"
#include "Snake.hpp"

class Game
{
public:
    static bool isAppleLegal(const Cell& apple, const Snake& snake);
    static void generateApple(Cell& apple, const Snake& snake);
    static void eatApple(Cell& apple, Snake& snake);
    static void updateBoard(const Cell& apple, const Snake& snake);
    static void initBoard(Cell& apple, Snake& snake);
    static void set(const Cell& cell);

    static void print();

    static const int size = 30;
    static std::vector<std::vector<Cell>> board;
};