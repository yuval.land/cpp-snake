#include "Cell.hpp"
#include "Snake.hpp"
#include "Game.hpp"

void ncursesSetup()
{
    initscr();
    cbreak();
    noecho();
    nodelay(stdscr, 1);
    scrollok(stdscr, 1);
}

int main(void)
{
    ncursesSetup();

    Cell apple;
    Snake snake;
    int direction = 'W', oldDirection;

    Game::initBoard(apple, snake);

    while (true)
    {
        system("clear");

        oldDirection = direction;
        if ((direction = getch()) == -1)
            direction = oldDirection;
        flushinp();

        snake.move(direction);

        if (snake.head == apple)
            Game::eatApple(apple, snake);

        if (snake.checkDeath(Game::size))
            break;

        Game::updateBoard(apple, snake);

        Game::print();
        std::cout << "\n" << std::string(Game::size - 8, ' ') << "Score: " << snake.score << "\r\n";

        // Wait .5 seconds
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }

    Game::print();

    printf("You died!");
    getchar();

    endwin();

    return 0;
}